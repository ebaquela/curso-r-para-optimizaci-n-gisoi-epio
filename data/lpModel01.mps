*<meta creator='lp_solve v5.5'>
*<meta rows=3>
*<meta columns=4>
*<meta equalities=0>
*<meta integers=2>
*<meta origsense='MIN'>
*
NAME                  
ROWS
 N  R0      
 G  Rest. 01
 L  Rest. 02
 G  Rest. 03
COLUMNS
    X1        R0        1.0000000000   Rest. 02  0.2400000000
    X1        Rest. 03  12.680000000
    MARK0000  'MARKER'                 'INTORG'
    X2        R0        3.0000000000   Rest. 01  78.260000000
    MARK0001  'MARKER'                 'INTEND'
    X3        R0        6.2400000000   Rest. 02  11.310000000
    X3        Rest. 03  0.0800000000
    MARK0002  'MARKER'                 'INTORG'
    X4        R0        0.1000000000   Rest. 01  2.9000000000
    X4        Rest. 03  0.9000000000
    MARK0003  'MARKER'                 'INTEND'
RHS
    RHS       Rest. 01  92.300000000   Rest. 02  14.800000000
    RHS       Rest. 03  4.0000000000
BOUNDS
 LO BND       X1        28.600000000
 BV BND       X2      
 LO BND       X4        18.000000000
 UP BND       X4        48.980000000
ENDATA
