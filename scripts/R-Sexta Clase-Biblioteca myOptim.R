myOptim <- function (valIniciales, funcion, gradiente = NULL, ..., cotaInf = -Inf, 
                     cotaSup = Inf, listaControl = list(), hessiano = FALSE) {
  
  # Chequeo que m�todo usar
  if (length(valIniciales) == 1){
    metodo <- "Brent"
    if (cotaInf == -Inf){
      cotaInf <- -100000000000000
    }
    if (cotaSup == Inf){
      cotaSup <-  100000000000000
    }
  }
  else {
    if (is.null(gradiente)){
      metodo <- "Nelder-Mead"
    }
    else {
      metodo <- "BFGS"
    }
  }
  
  # Llamo a optim():
  print(paste("El m�todo utilizado fue: ", metodo))
  optim(valIniciales, funcion, gradiente, ..., method=metodo , lower = cotaInf, 
        upper = cotaSup, control = listaControl, hessian = hessiano)
  
}